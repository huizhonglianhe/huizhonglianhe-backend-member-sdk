<?php
namespace Sdk\Member\Enterprise\Adapter;

use Sdk\Member\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Member\Common\Adapter\FetchAbleRestfulAdapterTrait;

use Sdk\Member\Enterprise\Model\NullEnterprise;
use Sdk\Member\Enterprise\Translator\EnterpriseRestfulTranslator;
use Sdk\Member\Sdk;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Framework\Interfaces\ITranslator;

class EnterpriseRestfulAdapter extends GuzzleAdapter implements IEnterpriseAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait, FetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    public function __construct(string $uri, array $authKey)
    {
        parent::__construct(
           $uri,
           $authKey
        );
        $this->translator = new EnterpriseRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'enterprises';
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario): void
    {
        $this->scenario = $scenario;
    }

    protected function getTranslator() : ITranslator
    {
        return $this->translator;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullEnterprise());
    }
}