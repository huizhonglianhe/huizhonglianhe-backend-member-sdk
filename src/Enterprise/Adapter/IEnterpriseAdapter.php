<?php
namespace Sdk\Member\Enterprise\Adapter;

use Sdk\Member\Common\Adapter\IFetchAbleAdapter;

use Marmot\Framework\Interfaces\IAsyncAdapter;
use Marmot\Framework\Interfaces\IErrorAdapter;

interface IEnterpriseAdapter extends IAsyncAdapter, IFetchAbleAdapter, IErrorAdapter
{
}