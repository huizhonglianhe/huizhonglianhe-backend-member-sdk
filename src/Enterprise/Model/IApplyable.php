<?php
namespace Sdk\Member\Enterprise\Model;

interface IApplyable 
{
	public function apply() : bool
}