<?php
namespace Sdk\Member\Enterprise\Model;

/**
 * 联系人值对象,包含:
 * 联系人姓名 $name
 * 联系人手机号 $cellphone
 * 联系人地址 $address
 */
class Contacts
{

    /**
     * @var string $name 联系人姓名
     */
    private $name;
    /**
     * @var string $cellphone 联系人手机号
     */
    private $cellphone;
    /**
     * @var string $address 联系人地址
     */
    private $address;

    public function __construct(
        string $name = '',
        string $cellphone = '',
        string $address = ''
    ) 
    {
        $this->name = '';
        $this->cellphone = '';
        $this->address = '';
    }

    public function __destruct()
    {
        unset($this->name);
        unset($this->cellphone);
        unset($this->address);
    }

    public function getCellphone() : string
    {
        return $this->cellphone;
    }

    public function setAddress(string $address) : void
    {
        $this->address = $address;
    }

    public function getAddress() : string
    {
        return $this->address;
    }
}
