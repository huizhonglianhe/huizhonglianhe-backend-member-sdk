<?php
namespace Sdk\Member\Enterprise\Model;

use Marmot\Framework\Interfaces\INull;

class NullEnterprise extends Enterprise implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}