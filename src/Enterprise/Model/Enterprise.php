<?php
namespace Sdk\Member\Enterprise\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

use Sdk\Member\Member\Model\Member;

class Enterprise implements IObject
{
    use Object;

    const STATUS_NORMAL = 0;
    const STATUS_DELETE = -2;

    const AUDIT_STATUS = array(
        'PENDING' => 2,
        'APPROVE' => 4,
        'REJECT' => -4
    );

    /**
     * @var int $id id
     */
    private $id;
    /**
     * @var string $name 企业名称
     */
    private $name;
    /**
     * @var string $unifiedSocialCreditCode 统一社会信用代码
     */
    private $unifiedSocialCreditCode;
    /**
     * @var array $logo logo
     */
    private $logo;
    /**
     * @var array $businessLicense 营业执照
     */
    private $businessLicense;
    /**
     * @var Contacts $contacts 联系信息
     */
    private $contacts;
    /**
     * @var array $powerAttorney 授权委托书
     */
    private $powerAttorney;
    /**
     * @var Member $member 用户
     */
    private $member;
    /**
     * @var int $applyStatus 审核状态
     */
    private $applyStatus;
    /**
     * @var string 驳回原因
     */
    private $rejectReason;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->companyName = '';
        $this->unifiedSocialCreditCode = '';
        $this->logo = array();
        $this->certificates = array();
        $this->contacts = new Contacts();
        $this->powerAttorney = array();
        $this->member =  new Member();
        $this->statusTime = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = self::STATUS_NORMAL;
        $this->applyStatus = self::AUDIT_STATUS['PENDING'];
        $this->rejectReason = '';
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->companyName);
        unset($this->unifiedSocialCreditCode);
        unset($this->logo);
        unset($this->certificates);
        unset($this->contactName);
        unset($this->contactPhone);
        unset($this->contactsAddress);
        unset($this->powerAttorney);
        unset($this->member);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->status);
        unset($this->applyStatus);
        unset($this->reason);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setUnifiedSocialCreditCode(string $unifiedSocialCreditCode) : void
    {
        $this->unifiedSocialCreditCode = $unifiedSocialCreditCode;
    }

    public function getUnifiedSocialCreditCode() : string
    {
        return $this->unifiedSocialCreditCode;
    }

    public function setLogo(array $logo) : void
    {
        $this->logo = $logo;
    }

    public function getLogo() : array
    {
        return $this->logo;
    }

    public function setBusinessLicense(array $businessLicense) : void
    {
        $this->businessLicense = $businessLicense;
    }

    public function getBusinessLicense() : array
    {
        return $this->businessLicense;
    }

    public function setContacts(Contacts $contacts) : void
    {
        $this->contacts = $contacts;
    }

    public function getContacts() : Contacts
    {
        return $this->contacts;
    }

    public function setPowerAttorney(array $powerAttorney) : void
    {
        $this->powerAttorney = $powerAttorney;
    }

    public function getPowerAttorney() : array
    {
        return $this->powerAttorney;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setApplyStatus(int $applyStatus) : void
    {
        $this->applyStatus = $applyStatus;
    }

    public function getApplyStatus() : int
    {
        return $this->applyStatus;
    }

    public function isApplyPending() : bool
    {
        return $this->applyStatus == self::AUDIT_STATUS['PENDING'];
    }

    public function isApplyApproved() : bool
    {
        return $this->applyStatus == self::AUDIT_STATUS['APPROVE'];
    }

    public function isApplyRejected() : bool
    {
        return $this->applyStatus == self::AUDIT_STATUS['REJECT'];
    }

    public function setStatus(int $status) : void
    {
        $this->status= in_array(
            $status,
            array(
                self::STATUS_NORMAL,
                self::STATUS_DELETE
            )
        ) ? $status : self::STATUS_NORMAL;
    }

    public function setRejectReason(string $rejectReason) : void
    {
        $this->rejectReason = $rejectReason;
    }

    public function getRejectReason() : string
    {
        return $this->rejectReason;
    }
}
