<?php
namespace Sdk\Member\Enterprise\Translator;

use Sdk\Member\Member\Translator\MemberRestfulTranslator;
use Sdk\Member\Common\Translator\RestfulTranslatorTrait;

use Sdk\Member\Enterprise\Model\Enterprise;
use Sdk\Member\Enterprise\Model\NullEnterprise;
use Sdk\Member\Enterprise\Model\Contacts;

use Marmot\Framework\Interfaces\ITranslator;

class EnterpriseRestfulTranslator implements ITranslator
{
    use RestfulTranslatorTrait;

    protected function getMemberRestfulTranslator() : MemberRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }

    public function arrayToObject(array $expression, $enterprise = null)
    {
        return $this->translateToObject($expression, $enterprise);
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $enterprise = null)
    {
        if (empty($expression)) {
            return new NullEnterprise();
        }

        if ($enterprise == null) {
            $enterprise = new Enterprise();
        }

        $data =  $expression['data'];

        $id = $data['id'];
        $enterprise->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['name'])) {
            $enterprise->setName($attributes['name']);
        }
        if (isset($attributes['logo'])) {
             $enterprise->setLogo($attributes['logo']);
        }
        if (isset($attributes['businessLicense'])) {
            $enterprise->setBusinessLicense($attributes['businessLicense']);
        }
        if (isset($attributes['unifiedSocialCreditCode'])) {
            $enterprise->setUnifiedSocialCreditCode($attributes['unifiedSocialCreditCode']);
        }

        $contacts = new Contacts(
            isset($attributes['contactsName']) ? $attributes['contactsName'] : '',
            isset($attributes['contactsCellphone']) ? $attributes['contactsCellphone'] : '',
            isset($attributes['contactsAddress']) ? $attributes['contactsAddress'] : ''
        );
        $enterprise->setContacts($contacts);

        if (isset($attributes['powerAttorney'])) {
            $enterprise->setPowerAttorney($attributes['powerAttorney']);
        }
        if (isset($attributes['rejectReason'])) {
            $enterprise->setRejectReason($attributes['rejectReason']);
        }
        if (isset($attributes['applyStatus'])) {
            $enterprise->setApplyStatus($attributes['applyStatus']);
        }
        if (isset($attributes['status'])) {
            $enterprise->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $enterprise->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $enterprise->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $enterprise->setUpdateTime($attributes['updateTime']);
        }

        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($expression['included'])) {
            $relationships = $this->relationship($expression['included'], $relationships);
        }

        if (isset($relationships['member']['data'])) {
            $member = $this->changeArrayFormat($relationships['member']['data']);
            $enterprise->setMember($this->getMemberRestfulTranslator()->arrayToObject($member));
        }

        return $enterprise;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($enterprise, array $keys = array())
    {
        if (!$enterprise instanceof Enterprise) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'unifiedSocialCreditCode',
                'name',
                'logo',
                'businessLicense',
                'contacts',
                'powerAttorney',
                'member'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'enterprises'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $enterprise->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $enterprise->getCompanyName();
        }
        if (in_array('unifiedSocialCreditCode', $keys)) {
            $attributes['unifiedSocialCreditCode'] = $enterprise->getUnifiedSocialCreditCode();
        }
        if (in_array('logo', $keys)) {
            $attributes['logo'] = $enterprise->getLogo();
        }
        if (in_array('businessLicense', $keys)) {
            $attributes['businessLicense'] = $enterprise->getBusinessLicense();
        }
        if (in_array('contacts', $keys)) {
            $attributes['contactsName'] = $enterprise->getContacts()->getContactName();
            $attributes['contactsCellphone'] = $enterprise->getContacts()->getContactPhone();
            $attributes['contactsAddress'] = $enterprise->getContacts()->getContactsAddress();
        }
        if (in_array('powerAttorney', $keys)) {
            $attributes['powerAttorney'] = $enterprise->getPowerAttorney();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('member', $keys)) {
            $expression['data']['relationships']['member']['data'] = array(
                array(
                    'type' => 'members',
                    'id' => $enterprise->getMember()->getId()
                )
            );
        }

        return $expression;
    }
}
