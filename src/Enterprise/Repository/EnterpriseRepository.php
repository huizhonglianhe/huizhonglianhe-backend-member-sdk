<?php
namespace Sdk\Member\Enterprise\Repository;

use Sdk\Member\Common\Repository\AsyncRepositoryTrait;
use Sdk\Member\Common\Repository\FetchRepositoryTrait;
use Sdk\Member\Common\Repository\ErrorRepositoryTrait;

use Sdk\Member\Enterprise\Adapter\IEnterpriseAdapter;
use Sdk\Member\Enterprise\Adapter\EnterpriseRestfulAdapter;
use Sdk\Member\Enterprise\Model\Enterprise;

use Marmot\Framework\Interfaces\INull;
use Marmot\Framework\Interfaces\ISdk;

class EnterpriseRepository implements IEnterpriseAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, ErrorRepositoryTrait;

    private $adapter;

    public function __construct(string $uri, array $authKey)
    {
        $this->adapter = new EnterpriseRestfulAdapter(
            $uri,
            $authKey
        );
    }

    protected function getAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
