<?php
namespace Sdk\Member\Enterprise\Service;

use Sdk\Member\Enterprise\Model\Enterprise;
use Sdk\Member\Enterprise\Model\IApplyable;

abstract class AuthenicateService
{
	const FAIL_STATUS = array(
		'NOT_DEFINED' => 0
        'ENTERPRISE_APPLY_STATUS_NOT_APPROVED' => -2
    );

	private $lastError;

	public function __construct(
		Enterprise $enterprise,
		IApplyable $applyform
	)
	{
		$this->enterprise = $enterprise;
		$this->applyform = $applyform;
		$this->lastError = self::FAIL_STATUS['NOT_DEFINED'];
	}

	public function authenticate()
	{
		if (!$enterprise->isApplyApproved()) {
			$this->lastError = self::FAIL_STATUS['ENTERPRISE_APPLY_STATUS_NOT_APPROVED'];
			return $this->errorHandler();
		}

		return $applyform->apply();
	}

	protected function getLastError() : int
	{
		return $this->lastError;
	}

	protected abstract function errorHandler() : void;
}