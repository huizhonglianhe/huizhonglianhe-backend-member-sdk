<?php
namespace Sdk\Member\User\Translator;

use Sdk\Member\Common\Translator\RestfulTranslatorTrait;

use Sdk\Member\User\Model\User;

abstract class UserRestfulTranslator
{
    use RestfulTranslatorTrait;

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function translateToObject(array $expression, $user = null)
    {
        $data =  $expression['data'];

        $id = $data['id'];

        $user->setId($id);

        $attributes = isset($data['attributes']) ? $data['attributes'] : '';

        if (isset($attributes['cellphone'])) {
            $user->setCellphone($attributes['cellphone']);
        }
        if (isset($attributes['realName'])) {
            $user->setRealName($attributes['realName']);
        }
        if (isset($attributes['userName'])) {
            $user->setUserName($attributes['userName']);
        }
        if (isset($attributes['password'])) {
            $user->setPassword($attributes['password']);
        }
        if (isset($attributes['avatar'])) {
            $user->setAvatar($attributes['avatar']);
        }
        if (isset($attributes['createTime'])) {
            $user->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $user->setUpdateTime($attributes['updateTime']);
        }
        if (isset($attributes['status'])) {
            $user->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $user->setStatusTime($attributes['statusTime']);
        }

        return $user;
    }

    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function objectToArray($user, array $keys = array())
    {
        $expression = array();

        if (!$user instanceof User) {
            return $expression;
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'cellphone',
                'passport',
                'realName',
                'userName',
                'password',
                'oldPassword',
                'avatar'
            );
        }

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $user->getId();
        }

        $attributes = array();

        if (in_array('cellphone', $keys)) {
            $attributes['cellphone'] = $user->getCellphone();
        }
        if (in_array('passport', $keys)) {
            $attributes['passport'] = $user->getCellphone();
        }
        if (in_array('realName', $keys)) {
            $attributes['realName'] = $user->getRealName();
        }
        if (in_array('userName', $keys)) {
            $attributes['userName'] = $user->getUserName();
        }
        if (in_array('password', $keys)) {
            $attributes['password'] = $user->getPassword();
        }
        if (in_array('oldPassword', $keys)) {
            $attributes['oldPassword'] = $user->getOldPassword();
        }
        if (in_array('avatar', $keys)) {
            $attributes['avatar'] = $user->getAvatar();
        }

        $expression['data']['attributes'] = $attributes;

        return $expression;
    }
}
