<?php
namespace Sdk\Member\User\Model;

use Marmot\Common\Model\IObject;
use Marmot\Common\Model\Object;

abstract class User implements IObject
{
    use Object;

    protected $id;

    protected $cellphone;

    protected $realName;
 
    protected $userName;

    protected $password;

    protected $oldPassword;

    protected $avatar;

    public function __construct(int $id = 0)
    {
        $this->id = $id;
        $this->cellphone = '';
        $this->realName = '';
        $this->userName = '';
        $this->password = '';
        $this->oldPassword = '';
        $this->avatar = array();
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->status = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->cellphone);
        unset($this->realName);
        unset($this->userName);
        unset($this->password);
        unset($this->oldPassword);
        unset($this->avatar);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->status);
        unset($this->statusTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setCellphone(string $cellphone) : void
    {
        $this->cellphone = is_numeric($cellphone) ? $cellphone : '';
    }

    public function getCellphone() : string
    {
        return $this->cellphone;
    }

    public function setRealName(string $realName) : void
    {
        $this->realName = $realName;
    }

    public function getRealName() : string
    {
        return $this->realName;
    }

    public function setUserName(string $userName) : void
    {
        $this->userName = $userName;
    }

    public function getUserName() : string
    {
        return $this->userName;
    }

    public function setPassword(string $password) : void
    {
        $this->password = $password;
    }

    public function getPassword() : string
    {
        return $this->password;
    }

    public function setOldPassword(string $oldPassword) : void
    {
        $this->oldPassword = $oldPassword;
    }

    public function getOldPassword() : string
    {
        return $this->oldPassword;
    }
    
    public function setAvatar(array $avatar) : void
    {
        $this->avatar = $avatar;
    }

    public function getAvatar() : array
    {
        return $this->avatar;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
