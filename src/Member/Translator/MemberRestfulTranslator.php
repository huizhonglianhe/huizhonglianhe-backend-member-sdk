<?php
namespace Sdk\Member\Member\Translator;

use Sdk\Member\Member\Model\Member;
use Sdk\Member\Member\Model\NullMember;

use Sdk\Member\Common\Translator\RestfulTranslatorTrait;
use Sdk\Member\User\Translator\UserRestfulTranslator;

use Marmot\Framework\Interfaces\ITranslator;

class MemberRestfulTranslator extends UserRestfulTranslator implements ITranslator
{
    use RestfulTranslatorTrait;

    public function arrayToObject(array $expression, $member = null)
    {
        return $this->translateToObject($expression, $member);
    }

    public function translateToObject(array $expression, $member = null)
    {
        if (empty($expression)) {
            return new NullMember();
        }

        if ($member == null) {
            $member = new Member();
        }

        $member = parent::translateToObject($expression, $member);

        $data =  $expression['data'];

        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        if (isset($attributes['authenticatingState'])) {
            $member->setAuthenticatingState($attributes['authenticatingState']);
        }
        return $member;
    }

    public function objectToArray($member, array $keys = array())
    {
        $user = parent::objectToArray($member, $keys);

        if (!$member instanceof Member) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'authenticatingState'
            );
        }

        $expression = array(
            'data'=>array(
                'type'=>'members',
                'id'=>$member->getId()
            )
        );

        $attributes = array();

        if (in_array('authenticatingState', $keys)) {
            $attributes['authenticatingState'] = $member->getAuthenticatingState();
        }

        $expression['data']['attributes'] = array_merge($user['data']['attributes'], $attributes);

        return $expression;
    }
}
