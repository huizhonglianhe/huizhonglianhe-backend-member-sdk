<?php
namespace Sdk\Member\Member\Model;

use Marmot\Core;
use Marmot\Framework\Interfaces\INull;

class NullMember extends Member implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
