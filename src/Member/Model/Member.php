<?php
namespace Sdk\Member\Member\Model;

use Marmot\Core;

use Sdk\Member\Member\Repository\MemberRepository;

use Sdk\Member\User\Model\User;

class Member extends User
{
    const AUTHENTICATING_STATE = array(
        "UNCERTIFIED" => 0,
        "CERTIFIED" => 2
    );

    protected $id;

    private $authenticatingState;
    
    private $identify;

    public function __construct($id = 0)
    {
        parent::__construct();
        $this->id = $id;
        $this->authenticatingState = self::AUTHENTICATING_STATE['UNCERTIFIED'];
        $this->identify = '';
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->id);
        unset($this->authenticatingState);
        unset($this->identify);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setAuthenticatingState(int $authenticatingState) : void
    {
        $this->authenticatingState = $authenticatingState;
    }

    public function getAuthenticatingState()
    {
        return $this->authenticatingState;
    }

    public function setIdentify(string $identify) : void
    {
        $this->identify = $identify;
    }

    public function getIdentify() : string
    {
        return $this->identify;
    }
}