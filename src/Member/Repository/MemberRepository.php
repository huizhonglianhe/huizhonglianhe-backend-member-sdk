<?php
namespace Sdk\Member\Member\Repository;

use Sdk\Member\Common\Repository\AsyncRepositoryTrait;
use Sdk\Member\Common\Repository\FetchRepositoryTrait;
use Sdk\Member\Common\Repository\ErrorRepositoryTrait;

use Sdk\Member\Member\Adapter\IMemberAdapter;
use Sdk\Member\Member\Adapter\MemberRestfulAdapter;
use Sdk\Member\Member\Model\Member;

use Marmot\Framework\Interfaces\INull;
use Marmot\Framework\Interfaces\ISdk;

class MemberRepository implements IMemberAdapter
{
    use FetchRepositoryTrait, AsyncRepositoryTrait, ErrorRepositoryTrait;

    private $adapter;

    public function __construct(string $uri, array $authKey)
    {
        $this->adapter = new MemberRestfulAdapter(
            $uri,
            $authKey
        );
    }

    protected function getAdapter()
    {
        return $this->adapter;
    }

    public function scenario($scenario)
    {
        $this->getAdapter()->scenario($scenario);
        return $this;
    }
}
