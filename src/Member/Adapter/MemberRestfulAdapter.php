<?php
namespace Sdk\Member\Member\Adapter;

use Sdk\Member\Common\Adapter\AsyncFetchAbleRestfulAdapterTrait;
use Sdk\Member\Common\Adapter\FetchAbleRestfulAdapterTrait;

use Sdk\Member\Member\Model\NullMember;
use Sdk\Member\Member\Translator\MemberRestfulTranslator;
use Sdk\Member\Sdk;

use Marmot\Framework\Adapter\Restful\GuzzleAdapter;
use Marmot\Framework\Interfaces\ITranslator;

class MemberRestfulAdapter extends GuzzleAdapter implements IMemberAdapter
{
    use AsyncFetchAbleRestfulAdapterTrait, FetchAbleRestfulAdapterTrait;

    private $translator;

    private $resource;

    public function __construct(string $uri, array $authKey)
    {
        parent::__construct(
           $uri,
           $authKey
        );
        $this->translator = new MemberRestfulTranslator();
        $this->scenario = array();
        $this->resource = 'members';
    }

    protected function getResource() : string
    {
        return $this->resource;
    }

    public function scenario($scenario): void
    {
        $this->scenario = $scenario;
    }

    protected function getTranslator() : ITranslator
    {
        return $this->translator;
    }

    public function fetchOne($id)
    {
        return $this->fetchOneAction($id, new NullMember());
    }
}