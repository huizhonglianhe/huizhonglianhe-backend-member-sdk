<?php
namespace Sdk\Member\Member\Adapter;

use Sdk\Member\Common\Adapter\IFetchAbleAdapter;

use Marmot\Framework\Interfaces\IAsyncAdapter;
use Marmot\Framework\Interfaces\IErrorAdapter;

interface IMemberAdapter extends IAsyncAdapter, IFetchAbleAdapter, IErrorAdapter
{
}
