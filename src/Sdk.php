<?php
namespace Sdk\Member;

use Marmot\Framework\Interfaces\ISdk;

use Sdk\Member\Member\Adapter\IMemberAdapter;
use Sdk\Member\Enterprise\Adapter\IEnterpriseAdapter;

use Sdk\Member\Member\Repository\MemberRepository;
use Sdk\Member\Enterprise\Repository\EnterpriseRepository;

class Sdk implements ISdk
{
	private $uri;

	private $authKey;

	private $memberRepository;

	private $enterpriseRepository;

	public function __construct(string $uri, array $authKey)
	{
		$this->uri = $uri;
		$this->authKey = $authKey;
		$this->memberRepository = new MemberRepository($uri, $authKey);
		$this->enterpriseRepository = new EnterpriseRepository($uri, $authKey);
	}

	public function __destruct()
	{
		unset($this->uri);
		unset($this->authKey);
		unset($this->memberRepository);
		unset($this->enterpriseRepository);
	}

	public function getUri() : string
	{
		return $this->uri;
	}

	public function getAuthKey() : array
	{
		return $this->authKey;
	}

	public function memberRepository() : IMemberAdapter
	{
		return $this->memberRepository;
	}

	public function enterpriseRepository() : IEnterpriseAdapter
	{
		return $this->enterpriseRepository;
	}
}