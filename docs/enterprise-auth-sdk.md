# 企业认证

## 设计思路

我们抽象为:

* 企业去做认证
* 所有的认证必须去填写一个表单然后申请
* 认证抽象为一个**领域服务**

![](docs/images/AuthenicateService.png)

### 如何开发

#### 1. 实现自己的申请认证接口

```php 
Sdk\Member\Enterprise\Model\IApplyable;
```

`implements`实现`apply()`函数, 实现自己的申请认证.

#### 2. 继承认证领域服务

```
use Sdk\Member\Enterprise\Service\AuthenicateService;

xxx extends AuthenicateService
{
...
}
```

需要实现`errorHandler()`函数, 即认证失败后自己内部的处理机制. 调用`getLastError()`可以获取认证失败信息.

### 代码

**IApplyable.php**

```
<?php
namespace Sdk\Member\Enterprise\Model;

interface IApplyable 
{
	public function apply() : bool
}
```

**AuthenicateService.php**

```
<?php
namespace Sdk\Member\Enterprise\Service;

use Sdk\Member\Enterprise\Model\Enterprise;
use Sdk\Member\Enterprise\Model\IApplyable;

abstract class AuthenicateService
{
	const FAIL_STATUS = array(
		'NOT_DEFINED' => 0
        'ENTERPRISE_APPLY_STATUS_NOT_APPROVED' => -2
    );

	private $lastError;

	public function __construct(
		Enterprise $enterprise,
		IApplyable $applyform
	)
	{
		$this->enterprise = $enterprise;
		$this->applyform = $applyform;
		$this->lastError = self::FAIL_STATUS['NOT_DEFINED'];
	}

	public function authenticate()
	{
		if (!$enterprise->isApplyApproved()) {
			$this->lastError = self::FAIL_STATUS['ENTERPRISE_APPLY_STATUS_NOT_APPROVED'];
			return $this->errorHandler();
		}

		return $applyform->apply();
	}

	protected function getLastError() : int
	{
		return $this->lastError;
	}

	abstract protected function errorHandler() : void;
}
```