# 企业接口示例

## 目录

* [示例](#sdk-example)
	* 获取单个企业
	* 获取多个企业
	* 搜索企业

### <a name="sdk-example">示例</a>

#### 1. 获取单个企业

```php
$enterprise = $sdk->enterpriseRepository()->fetchOne(1);
```

#### 2. 获取多个企业

```
$enterprises = $sdk-> enterpriseRepository()->fetchList([1,2]);
```

#### 3. 搜索企业

```
$enterprises = $sdk-> enterpriseRepository()->search(array('unifiedSocialCreditCode'=>'统一社会信用代码'));
```