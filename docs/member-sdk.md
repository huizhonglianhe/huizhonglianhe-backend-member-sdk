# 用户接口示例

## 目录

* [示例](#sdk-example)
	* 获取单个用户
	* 获取多个用户
	* 搜索用户

### <a name="sdk-example">示例</a>

#### 1. 获取单个用户

```php
$member = $sdk->memberRepository()->fetchOne(1);
```

#### 2. 获取多个用户

```
$members = $sdk->memberRepository()->fetchList([1,10]);
```

#### 3. 搜索用户

```
$member = $sdk->memberRepository()->scenario(array('fields'=>['members'=>'cellphone']))->search(array('cellphone'=>'15909287779'));
```